# frozen_string_literal: true

# Extra elements a vehicle can have
class Extras
  attr_accessor :price

  def initialize(price)
    @price = price
  end

  def to_s; end
end

# Radio that can be added into a vehicle
class Radio < Extras
  def to_s
    "Radio ($#{@price})"
  end
end

# AC that can be added into a vehicle
class AC < Extras
  def to_s
    "AC ($#{@price})"
  end
end

# Sunroof that can be added into a vehicle
class Sunroof < Extras
  def to_s
    "Sunroof ($#{@price})"
  end
end

# Leather Seats that can be added into a vehicle
class LeatherSeats < Extras
  def to_s
    "Leather Seats ($#{@price})"
  end
end

# Power Windows that can be added into a vehicle
class PowerWindows < Extras
  def to_s
    "Power Windows ($#{@price})"
  end
end
