# Car sales store (POO)

Made in Ruby 2.7.4

## Execution

Examples provided in vehicle.rb and store.rb

You can execute the program as:

```shell
(venv) $ ruby vehicle.rb
```

```shell
(venv) $ ruby store.rb
```