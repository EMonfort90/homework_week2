# frozen_string_literal: true

require_relative 'extras'
require 'securerandom'

# Class defining vehicle and its behaviour
class Vehicle
  attr_accessor :wheels_number, :color, :brand, :price, :extras
  attr_reader :id

  def initialize(wheels_number: 4, color: 'unkown', brand: 'unknown', price: -1)
    @wheels_number = wheels_number
    @color = color
    @brand = brand
    @price = price.to_f
    @id = SecureRandom.uuid
    @extras = []
  end

  def add_extras(extra)
    @extras << extra
  end

  def show_extras
    puts 'Extras:'
    @extras.each { |extra| puts "\t#{extra}" }
  end

  def show_details
    puts 'Detail:'
    puts "\tQuote for the car: #{id}"
    puts "\tFeatures"
    puts "\tcolor: #{@color}"
    puts "\tbrand: #{@brand}"
    puts "\twheels: #{@wheels_number}"
    puts "\tprice: $#{@price}"
  end

  def to_s
    "#{@brand} - #{@color} - $#{price} - id: #{id}"
  end

  def self.make_five_of_each
    vehicles = []
    ObjectSpace.each_object(Class) do |c|
      5.times { vehicles << c.new } if c < self
    end
    vehicles
  end
end

# Vehicle descendant representing a car
class Car < Vehicle
  def to_s
    "Car: #{super}"
  end
end

# Vehicle descendant representing a truck
class Truck < Vehicle
  def to_s
    "Truck: #{super}"
  end
end

my_car = Car.new(wheels_number: 4, color: 'black', brand: 'Mercedes', price: 35_000)
radio = Radio.new(50)
leather_seats = LeatherSeats.new(450)
my_car.add_extras(radio)
my_car.add_extras(leather_seats)
puts 'Show car and its extras:'
puts my_car
my_car.show_extras
puts '-' * 100
puts 'Make five o each vehicles:'
vehicle = Vehicle.make_five_of_each
puts vehicle
puts '-' * 100
