# frozen_string_literal: true

require_relative 'vehicle'

# Vehicle store class
# Can add, remove, show vehicles in store, and quote a vehicle
class Store
  attr_accessor :vehicles

  def initialize
    @vehicles = {}
  end

  def add_vehicle(vehicle)
    @vehicles[vehicle.id] = vehicle
  end

  def remove_vehicle(vehicle)
    @vehicles.delete(vehicle)
  end

  def get_vehicle(id)
    @vehicles[id]
  end

  def show_vehicles
    puts 'Vehicles in stock:'
    vehicles.each_value { |vehicle| puts vehicle }
  end

  def order_detail(vehicle, extras)
    total = vehicle.price
    vehicle.show_details
    puts '-' * 65
    puts 'Extras:'
    extras.each do |extra|
      total += extra.price
      puts "\t#{extra}"
    end
    puts '-' * 65
    puts "\tTotal: #{total}"
  end
end

my_mercedes = Car.new(wheels_number: 4, color: 'black', brand: 'Mercedes', price: 35_000)
my_chevrolet = Truck.new(wheels_number: 6, color: 'white', brand: 'Chevrolet', price: 65_000)
my_store = Store.new
my_store.add_vehicle(my_mercedes)
my_store.add_vehicle(my_chevrolet)
my_store.show_vehicles
puts '-' * 100
my_car = Car.new(wheels_number: 4, color: 'white', brand: 'Ford', price: 28_000)
my_extras = [Radio.new(50), LeatherSeats.new(900), AC.new(450)]
puts 'Show order detail:'
my_store.order_detail(my_car, my_extras)
