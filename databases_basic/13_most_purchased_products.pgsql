SELECT products.id, products.name, SUM(order_lines.quantity) AS total_purchases FROM products
JOIN order_lines
ON products.id = order_lines.product_id
GROUP BY products.id
HAVING products.id IN (
  SELECT product_id FROM order_lines
  GROUP BY product_id
  HAVING SUM(quantity) = (
    SELECT SUM(QUANTITY) AS total FROM order_lines
    GROUP BY product_id
    ORDER BY total DESC
    LIMIT 1
  )
);
