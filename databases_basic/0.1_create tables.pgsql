DROP TABLE IF EXISTS order_lines;
DROP TABLE IF EXISTS orders;
DROP TABLE IF EXISTS customers;
DROP TABLE IF EXISTS products;
DROP TYPE IF EXISTS STATUS_T;

CREATE TYPE STATUS_T AS ENUM('canceled', 'completed', 'failed', 'on hold',
                              'pending payment', 'processing', 'refunded');

CREATE TABLE customers(
  id SERIAL PRIMARY KEY,
  first_name VARCHAR(50),
  last_name VARCHAR(50),
  address VARCHAR(50),
  phone VARCHAR(20)
);

CREATE TABLE orders(
  id SERIAL PRIMARY KEY,
  date DATE,
  customer_id INTEGER,
  total REAL,
  status STATUS_T,
  FOREIGN KEY(customer_id) REFERENCES customers(id) ON DELETE CASCADE
);

CREATE TABLE products(
  id SERIAL PRIMARY KEY,
  sku VARCHAR(20),
  name VARCHAR(30),
  description VARCHAR(100),
  price REAL,
  stock INTEGER
);

CREATE TABLE order_lines(
  order_id INTEGER,
  product_id INTEGER,
  quantity INTEGER,
  price REAL,
  total REAL,
  FOREIGN KEY(order_id) REFERENCES orders(id) ON DELETE CASCADE,
  FOREIGN KEY(product_id) REFERENCES products(id) ON DELETE CASCADE
);