SELECT * FROM products
WHERE id IN (
  SELECT product_id FROM order_lines
  WHERE order_id IN (
    SELECT id FROM orders
    WHERE customer_id = 2
  )
);