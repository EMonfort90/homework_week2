SELECT * FROM customers
WHERE id IN (
  SELECT customer_id FROM orders
  WHERE id IN (
    SELECT order_id FROM order_lines
    WHERE product_id = 7
  )
);