# Database basics

## Using the following database diagram
![alt text](inventory.png)
* Create the SQL Queries required to replicate the DB Schema from the image in your DBMS
* Make sure to create the relations trough the tables
* Insert at least 20 products, 5 customers, 10 orders with their order details. Make sure to populate the database with random (but realistic) data!

## When the database is populated, make the following queries
1. Select All Products
2. Select All Clients
3. Select from what line an order belongs
4. Select All orders from an Order line
5. Select All orders a product has
6. Select the total of sales of X product
7. Update a Product price by 1.5
8. Select All the customers who bought an X product
9. Select All orders between dates X and Y
10. Select All products with price greater than 4.5
11. Select All the products a Customer has bought
12. Select the total amount of products a X customer bought between 2 dates
13. Select what is the most purchased product
14. Delete an order

## The RDBMS used to solve this problems is PostgreSQL