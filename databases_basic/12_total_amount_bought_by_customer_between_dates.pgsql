SELECT SUM(quantity) AS total_products FROM order_lines
WHERE order_id IN (
  SELECT id FROM orders
  WHERE customer_id = 2 AND date > '2021-09-01' AND date < '2021-11-15'
);